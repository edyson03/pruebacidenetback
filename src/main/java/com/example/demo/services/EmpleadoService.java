package com.example.demo.services;

import java.util.List;

import com.example.demo.entities.Empleado;

public interface EmpleadoService {
	
	public List<Empleado> findAll();
	
	public Empleado findById(Integer id);
	
	public Empleado save(Empleado empleado);
	
	public void delete(Integer id);

}
