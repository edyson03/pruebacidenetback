package com.example.demo.controller;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entities.Empleado;
import com.example.demo.services.EmpleadoService;

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/empleado")
public class EmpleadoControlador  {

	@Autowired
	private EmpleadoService empleadoService;
	
	@GetMapping("/listar")
	public List<Empleado> index(){
		return empleadoService.findAll();
	}
	
	@GetMapping("/empleado/{id}")
	public Empleado show(@PathVariable Integer id) {
		return empleadoService.findById(id);
	}
	
	@PostMapping("/registrar")
	@ResponseStatus(HttpStatus.CREATED)
	public Empleado registrar(@RequestParam Empleado empleado) {
		return empleadoService.save(empleado);
	}
	
	@PutMapping("/empleado/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public Empleado update(@RequestBody Empleado empleado, @PathVariable Integer id) {
		Empleado empleadoActual = empleadoService.findById(id);
		empleadoActual.setNombre(empleado.getNombre());
		empleadoActual.setOtroNombre(empleado.getOtroNombre());
		empleadoActual.setPrimerApellido(empleado.getPrimerApellido());
		empleadoActual.setSegundoApellido(empleado.getSegundoApellido());
		empleadoActual.setTipoIdentificacion(empleado.getTipoIdentificacion());
		empleadoActual.setNumeroIdentificacion(empleado.getNumeroIdentificacion());
		empleadoActual.setPais(empleado.getPais());
		empleadoActual.setEstado(empleado.getEstado());
		String dominio = "";
		if (empleado.getPais().equalsIgnoreCase("Colombia")) {
			dominio = "@cidenet.com.co";
		} else {
			dominio = "@cidenet.com.us";
			
		}
		empleado.setCorreo(empleado.getNombre() + empleado.getPrimerApellido() + dominio);
		empleadoActual.setCorreo(empleado.getCorreo());
		empleadoActual.setArea(empleado.getArea());
		return empleadoService.save(empleadoActual);
	}
	
	@DeleteMapping("/empleado/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Integer id) {
		empleadoService.delete(id);	
	}
	
}
