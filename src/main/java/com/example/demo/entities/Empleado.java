package com.example.demo.entities;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "empleados")
public class Empleado implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(length = 20)
	private String nombre;
	@Column(length = 50)
	private String otroNombre;
	@Column(length = 20)
	private String primerApellido;
	@Column(length = 20)
	private String segundoApellido;
	private String pais;
	private String tipoIdentificacion;
	@Column(length = 20)
	private String numeroIdentificacion;
	private String correo;	
	@Column(name = "fecha_hora_ingreso")
	private LocalDate fechaHoraIngreso;
	private String area;
	private String estado;
	
	public Empleado() {
		
	}
	
	public Empleado(int id, String nombre, String otroNombre, String primerApellido, String segundoApellido,
			String pais, String tipoIdentificacion, String numeroIdentificacion, String correo, LocalDate fechaHoraIngreso,
			String area, String estado) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.otroNombre = otroNombre;
		this.primerApellido = primerApellido;
		this.segundoApellido = segundoApellido;
		this.pais = pais;
		this.tipoIdentificacion = tipoIdentificacion;
		this.numeroIdentificacion = numeroIdentificacion;
		this.correo = correo;
		this.fechaHoraIngreso = fechaHoraIngreso;
		this.area = area;
		this.estado = estado;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getOtroNombre() {
		return otroNombre;
	}
	
	public void setOtroNombre(String otroNombre) {
		this.otroNombre = otroNombre;
	}
	
	public String getPrimerApellido() {
		return primerApellido;
	}
	
	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}
	
	public String getSegundoApellido() {
		return segundoApellido;
	}
	
	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}
	
	public String getPais() {
		return pais;
	}
	
	public void setPais(String pais) {
		this.pais = pais;
	}
	
	public String getTipoIdentificacion() {
		return tipoIdentificacion;
	}
	
	public void setTipoIdentificacion(String tipoIdentificacion) {
		this.tipoIdentificacion = tipoIdentificacion;
	}
	
	public String getNumeroIdentificacion() {
		return numeroIdentificacion;
	}
	
	public void setNumeroIdentificacion(String numeroIdentificacion) {
		this.numeroIdentificacion = numeroIdentificacion;
	}
	
	public String getCorreo() {
		return correo;
	}
	
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    public LocalDate getFechaIngreso() {
        return fechaHoraIngreso;
    }
	
	public void setFechaHoraIngreso(LocalDate fechaHoraIngreso) {
		this.fechaHoraIngreso = fechaHoraIngreso;
	}
	
	public String getArea() {
		return area;
	}
	
	public void setArea(String area) {
		this.area = area;
	}
	
	public String getEstado() {
		return estado;
	}
	
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
}
