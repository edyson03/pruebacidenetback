package com.example.demo.dao;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.entities.Empleado;

public interface EmpleadoRepository extends CrudRepository<Empleado, Integer>{

}
