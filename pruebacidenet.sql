-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-02-2021 a las 05:14:45
-- Versión del servidor: 10.1.35-MariaDB
-- Versión de PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `pruebacidenet`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleados`
--

CREATE TABLE `empleados` (
  `id` int(11) NOT NULL,
  `area` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `correo` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `estado` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `fecha_hora_ingreso` date DEFAULT NULL,
  `nombre` varchar(20) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `numero_identificacion` varchar(20) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `otro_nombre` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `pais` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `primer_apellido` varchar(20) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `segundo_apellido` varchar(20) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `tipo_identificacion` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `empleados`
--

INSERT INTO `empleados` (`id`, `area`, `correo`, `estado`, `fecha_hora_ingreso`, `nombre`, `numero_identificacion`, `otro_nombre`, `pais`, `primer_apellido`, `segundo_apellido`, `tipo_identificacion`) VALUES
(1, 'Administración', 'edyson@gmail.com', 'Activo', '2021-02-12', 'edyson', '1231', 'fabian', 'Colombia', 'leal', 'marin', 'Cedula');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `empleados`
--
ALTER TABLE `empleados`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `empleados`
--
ALTER TABLE `empleados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
